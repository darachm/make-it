
index.html: index.md 
	pandoc $< -s -o $@ -t revealjs \
		-V theme=simple -V transition=none --slide-level=2 

.PHONY: build
build: index.html
	mkdir -p html
	cp index.html html/index.html
	cp diagram.png html/
	cp -R reveal.js html

